function setSelectorAttributes(selector, attributes)
{
  if (Object.keys(attributes).length) {

    if (attributes.hasOwnProperty('data-p-x')) {
      $(selector).attr('data-p-x', attributes['data-p-x']);
    }

    if (attributes.hasOwnProperty('data-p-y')) {
      $(selector).attr('data-p-y', attributes['data-p-y']);
    }

    if (attributes.hasOwnProperty('data-x')) {
      $(selector).attr('data-x', attributes['data-x']);
    }

    if (attributes.hasOwnProperty('data-y')) {
      $(selector).attr('data-y', attributes['data-y']);
    }
  }
}

function renderMap(mx, my, px, py) {
  $('#map').clearCanvas();
  $('#map').drawRect({
    fillStyle: 'steelblue',
    fromCenter: false,
    x: parseInt(mx),
    y: parseInt(my),
    width: 999 - parseInt(mx),
    height: 999 - parseInt(my)
  }).drawRect({
    fillStyle: 'red',
    fromCenter: false,
    x: parseInt(px),
    y: parseInt(py),
    width: 10,
    height: 10
  });

}


$(document).on('click', '#generate-map', function(event){
  event.preventDefault();
  if (!$('#canvas-actions').hasClass('d-none')) {
    $('#canvas-actions').addClass('d-none')
  }
  $('#form-errors').remove();
  var data = $('#generate-map-form').serializeArray();

  $.ajax({
      url: "/generate/",
      method: "POST",
      data: data,
      success: function (response) {
        if (response.error) {
          $('#generate-map-form').after('<div class="alert alert-danger" id="form-errors" role="alert">'+response.error_message+'</div>');
        } else {
          $('#canvas-actions').toggleClass('d-none');
          renderMap(
            response.data.map.x_axis,
            response.data.map.y_axis,
            response.data.pointer.x_axis,
            response.data.pointer.y_axis
          );

          setSelectorAttributes(
            '#p-up',
            {
              'data-p-x': parseInt(response.data.map.x_axis),
              'data-p-y': parseInt(response.data.map.y_axis),
              'data-x': parseInt(response.data.pointer.x_axis),
              'data-y': parseInt(response.data.pointer.y_axis) + 1
            }
          );

          setSelectorAttributes(
            '#p-down',
            {
              'data-p-x': parseInt(response.data.map.x_axis),
              'data-p-y': parseInt(response.data.map.y_axis),
              'data-x': parseInt(response.data.pointer.x_axis),
              'data-y': parseInt(response.data.pointer.y_axis) - 1
            }
          );

          setSelectorAttributes(
            '#p-left',
            {
              'data-p-x': parseInt(response.data.map.x_axis),
              'data-p-y': parseInt(response.data.map.y_axis),
              'data-x': parseInt(response.data.pointer.x_axis) - 1,
              'data-y': parseInt(response.data.pointer.y_axis)
            }
          );

          setSelectorAttributes(
            '#p-right',
            {
              'data-p-x': parseInt(response.data.map.x_axis),
              'data-p-y': parseInt(response.data.map.y_axis),
              'data-x': parseInt(response.data.pointer.x_axis) + 1,
              'data-y': parseInt(response.data.pointer.y_axis)
            }
          );
        }
      },
  });
});

$(document).on('click', '.bi', function(e) {
  var mx = $(e.currentTarget).attr('data-p-x'),
      my = $(e.currentTarget).attr('data-p-y'),
      px = $(e.currentTarget).attr('data-x'),
      py = $(e.currentTarget).attr('data-y');

  renderMap(mx, my, px, py);
});