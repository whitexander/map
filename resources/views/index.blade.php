@extends('layouts.master')
@section('title', 'Map Homepage')
@section('content')
	<div class="row row-cols-2 justify-content-center">
		<div class="col-md-9">
			<div class="panel">
				<div class="panel-heading mb-3">
					<h4 class="panel-title text-center">Map</h3>
				</div>
				<div class="panel-body">
					<canvas id="map" width="1000" height="1000"></canvas>
					<div id="canvas-actions" class="d-none">
						<div class="row mt-3">
							<div class="fs-2 col-md-12 text-center">
								<a href="javascript:void(0)" id="p-up" class="bi bi-arrow-up-square"></a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="fs-2 text-end">
									<a href="javascript:void(0)" id="p-left" class="bi bi-arrow-left-square"></a>
								</div>
							</div>
							<div class="col-md-6">
								<div class="fs-2 text-start">
									<a href="javascript:void(0)" id="p-right" class="bi bi-arrow-right-square"></a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="fs-2 col-md-12 text-center">
								<a href="javascript:void(0)" id="p-down" class="bi bi-arrow-down-square"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-heading mb-3">
					<h4 class="panel-title">Set dimensions</h3>
			 	</div>
			 	<div class="panel-body">
				 	<form role="form" method="POST" action="/generate/" id="generate-map-form">
					 	@csrf
						<div class="row mb-3">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<div class="form-group">
									<input
										type="number"
										name="x_axis"
										id="x_axis" 
										class="form-control input-sm"
										placeholder="X Axis"
										max="999"
									/>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6">
								<div class="form-group">
									<input
										type="number"
										name="y_axis"
										id="y_axis"
										class="form-control input-sm"
										placeholder="Y Axis"
										max="999"
									/>
								</div>
							</div>
						</div>
						<div class="d-grid gap-2 mb-3">
							<input 
								type="submit" 
								value="Generate Map" 
								id="generate-map" 
								class="btn btn-secondary"
							/>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
