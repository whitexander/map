## About Project

This project is based on Laravel framework - [documentation](https://laravel.com/docs) .

## How to use it

Clone repository, create a copy of file .env.example and rename it to .env and in the end from command line run: php artisan serve.
