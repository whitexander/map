<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapController extends Controller
{
    /**
     * Display Map Page is used to render first time page with forms and buttons.
     *
     * @return void
     */
    public function displayMapPage()
    {
        return view('index');
    }

    /**
     * Generate map data
     *
     * @param Request $request
     * @return array
     */
    public function generateMapData(Request $request):array
    {
        $xAxis = $request->input('x_axis', '');
        $yAxis = $request->input('y_axis', '');
        $result = [
            'success' => false,
            'error' => false,
            'error_message' => '',
            'data' => []
        ];

        if (empty($xAxis) || empty($yAxis)) {
            if (empty($xAxis)) {
                $result['error'] = true;
                $result['error_message'] .= 'X Axis is empty. ';
            }

            if (empty($yAxis)) {
                $result['error'] = true;
                $result['error_message'] .= 'Y Axis is empty. ';
            }
        } elseif ($xAxis >= 1000 || $yAxis >= 1000) {
            if ($xAxis >= 1000) {
                $result['error'] = true;
                $result['error_message'] .= 'X Axis should values between 0 and 1000. ';
            }

            if ($yAxis >= 1000) {
                $result['error'] = true;
                $result['error_message'] .= 'Y Axis should values between 0 and 1000. ';
            }
        } else {
            $pointerPosition = \App\Http\Helpers\MapHelper::generatePinPostion(
                (int)$xAxis,
                (int)$yAxis
            );
            $result['success'] = true;
            $result['data'] = [
                'map' => [
                    'x_axis' => (int) $xAxis,
                    'y_axis' => (int) $yAxis
                ],
                'pointer' => $pointerPosition
            ];
        }

        return $result;
    }
}
