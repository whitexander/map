<?php

namespace App\Http\Helpers;

class MapHelper
{
    public static function generatePinPostion(int $xAxis, int $yAxis): array
    {
        return [
            'x_axis' => $xAxis + rand(0, $xAxis),
            'y_axis' => $yAxis + rand(0, $yAxis)
        ];
    }
}
